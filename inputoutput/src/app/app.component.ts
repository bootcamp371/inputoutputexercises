import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'inputoutput';
  books = ['Misery', 'Cujo', 'Book 3'];
  clickEdit(title: string) {
    console.log(`Editing book: ${title}`);
  }
}
